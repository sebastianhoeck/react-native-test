/**
 * Sample App
 */
import React, {Component} from 'react';
import {View} from 'react-native';
import Int from './src/Components/Int';
// import { WebView } from 'react-native-webview';
// import AgensDigitalLogo from './src/Components/AgensDigitalLogo';

export default class App extends Component {
  render() {
    return (
        <View style={{top: 200}}>
          <Int int={1} />
          <Int int={2} />
          <Int int={3} />
          <Int int={5} />
          <Int int={8} />
          <Int int={13} />
          <Int int={21} />
          <Int int={34} />
          <Int int={55} />
        </View>
    )
  }
}

// <WebView source={{uri: 'https://www.agens.digital/'}} />
/*
  <View style={{top: 200}}>
              <AgensDigitalLogo onlyText={true}/>
              <AgensDigitalLogo onlyText={false}/>
              <AgensDigitalLogo onlyText={true}/>
          </View>

 */


