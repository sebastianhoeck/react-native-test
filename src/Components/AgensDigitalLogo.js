import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';

const styles = StyleSheet.create({
    container: {
        marginTop: 50,
        width: '100%',
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: 'black',
        fontSize: 19
    },
    logo: {
        height: 20,
        width: 20
    }
});

class AgensDigitalLogo extends Component {

    // Default
    state = {
        onlyText: true,
        loaded: false
    };

    // Store props
    constructor( props ) {
        super( props );

        // Place your logic for the constructor here...
    }

    // Call if Component did mounted
    componentDidMount() {
        // Rerendering
        this.setState({onlyText: this.props.onlyText});

        const randomTimeout = 4000 + Math.random() * (4000 - 7000);

        // Simulate XHR
        setTimeout(() => {
            this.setState({loaded: true});
        }, randomTimeout);
    }

    // Press-Handler
    press() {
        alert('AgensDigital! :)');
    }

    render() {
        // Loading Screen
        if (this.state.loaded === false) {
            return (
                <View style={styles.container}>
                    <Text style={styles.text}>Lade...</Text>
                </View>
            );
        }
        // Without Logo
        else if (this.state.onlyText === true) {
            return (
                <View style={styles.container}>
                    <TouchableOpacity onPress={() => this.press()}>
                        <Text style={styles.text}>AgensDigital ohne Logo</Text>
                    </TouchableOpacity>
                </View>
            );
        // With Logo
        } else {
            return (
                <View style={styles.container}>
                    <TouchableOpacity onPress={() => this.press()}>
                        <Text style={styles.text}>AgensDigital mit</Text>
                    </TouchableOpacity>
                    <Image style={styles.logo} source={require('../Images/logo.png')} />
                </View>
            );
        }
    }
}

export default AgensDigitalLogo;
