import React, {Component} from 'react';
import {View, Text} from 'react-native';

class Counter extends Component {

    // Default
    state = {
        counter: 0
    };

    // Call if Component did mounted
    componentDidMount() {
        setTimeout(() => {
            this.setState({counter: this.state.counter++});
        }, 1000);
    }

    render() {
        return (
            <View>
                <Text>Counter: {this.state.counter}</Text>
            </View>
        )
    }
}

export default Counter;

// Verwendung: <Counter />
