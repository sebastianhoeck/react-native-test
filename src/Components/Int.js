import React, {Component} from 'react';
import {Text, TouchableOpacity} from 'react-native';

class Int extends Component {

    onPress() {
        alert(this.props.int);
    }

    render() {
        return (
            <TouchableOpacity onPress={() => this.onPress()}>
                <Text>{this.props.int}</Text>
            </TouchableOpacity>
        )
    }
}

export default Int;
